import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LayoutModule } from '@angular/cdk/layout';
import { MatIconModule } from '@angular/material';

import { AppMaterialModule } from './shared/material.module';
import { NavigationComponent } from './shared/navigation/navigation.component';
import { DashboardComponent } from './shared/dashboard/dashboard.component';
import { AccessDeniedComponent } from './error/access-denied/access-denied.component';
import { PageNotFoundComponent } from './error/page-not-found/page-not-found.component';
import { rootRouterConfig } from './app.routes';

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    DashboardComponent,
    AccessDeniedComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule.forRoot( rootRouterConfig, { enableTracing: false } ),
    LayoutModule,
    AppMaterialModule,
    MatIconModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
