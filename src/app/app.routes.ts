import { Routes } from '@angular/router';

import { AccessDeniedComponent } from './error/access-denied/access-denied.component';
import { PageNotFoundComponent } from './error/page-not-found/page-not-found.component';
import { DashboardComponent } from './shared/dashboard/dashboard.component';

export const rootRouterConfig: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: '',
    component: DashboardComponent,
    children: [
      {
        path: 'access-denied',
        component: AccessDeniedComponent,
        data: { title: 'Access Denied' }
      },
      {
        path: '404',
        component: PageNotFoundComponent,
        data: { title: '404: Page Not Found' }
      }
    ]
  },
  {
    path: '**',
    redirectTo: '404',
    pathMatch: 'full'
  }
];

